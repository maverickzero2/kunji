<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the public front page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('public.home');
    }

}
