<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Role\CreateRoleRequest;
use App\Http\Requests\Role\UpdateRoleRequest;
use App\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Softon\SweetAlert\Facades\SWAL;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param \App\Models\Role $role
     * @return \Illuminate\Http\Response
     */
    public function index(Role $role, Request $request)
    {
        return view('admin.roles.index')->with([
            'roles' => $role->all(),
            'user_id' => $request->user()->id,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.roles.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Role\CreateRoleRequest $request
     * @param  \App\Models\Role $role
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRoleRequest $request, Role $role)
    {
        $created = $role->fill($request->all())->save();

        if (!$created) {
            return back()
                ->withErrors(['Create' => "Failed to create new Role in the database. Please check your database log for more information."])
                ->withInput();
        }

        SWAL::success("Created Role: {$role->name}!");

        return redirect(route('admin.roles.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Role $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        return view('admin.roles.edit', ['role' => $role]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Role\UpdateRoleRequest
     * @param \App\Models\Role $role
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRoleRequest $request, Role $role)
    {
        // If not keys not received from form, then it's mean to be emptied.

        $data = $request->all();
        $data = data_fill($data, 'flags', []);

        $updated = $role->update($data);

        if (!$updated) {
            return back()
                ->withErrors(['Create' => "Failed to update Role: {$role->name} in the database. Please check your database log for more information."])
                ->withInput();
        }

        SWAL::success("Updated Role: {$role->name}!");

        return redirect(route('admin.roles.index'));
    }

    /**
     * Remove the specified resource from storage.
     * @param Role $role
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Role $role)
    {
        if ($role->delete()) {
            SWAL::success("Deleted Role {$role->name}!");
            return redirect(route('admin.roles.index'));
        }

        SWAL::error("Failed to delete Role: {$role->name}.. Contact the admin");

        return back();
    }

    /**
     * @param Request $request
     * @param Role $role
     * @return $this
     */

    public function trash(Request $request, Role $role)
    {
        return view('admin.roles.trash')->with([
            'roles' => $role->onlyTrashed()->get(),
            'user_id' => $request->user()->id,
        ]);
    }


    /**
     * @param int $id
     * @param Role $role
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function restore(string $name, Role $role)
    {

        $found = $role->onlyTrashed()->where('name', $name)->first();

        if (!empty($found)) {

            if ($restored = $role->onlyTrashed()->where('name', $name)->restore()) {

                SWAL::success("Restored Role {$found->name}!");
                return redirect(route('admin.roles.trash'));
            }

        }

        SWAL::error("Failed to restore Role: {$found->name}.. Contact the admin");
        return back();

    }

    /**
     * @param int $id
     * @param Role $role
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function erase(string $name, Role $role)
    {
        $found = $role->onlyTrashed()->where('name', $name)->first();

        if (!empty($found)) {


            if ($found->hasRelation()) {
                SWAL::warning("Unable to erased Role: {$found->name} because it's having relation with other object.");
                return back();
            }


            if ($erased = $role->onlyTrashed()->where('name', $name)->forceDelete()) {

                SWAL::success("Permanently erased Role {$found->name}!");
                return redirect(route('admin.roles.trash'));
            }

        }

        SWAL::error("Failed to erased Role: {$found->name}.. Contact the admin");
        return back();
    }


}
