<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // middleware verified
        // middleware activated
    }

    /**
     * Redirecting user to the respective dashboard according to role.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->user()->isAdmin()) {
            return redirect(route('admin.dashboard'));
        }

        if (!$request->user()->isVerified()) {
            return redirect(route('auth.unverified'));
        }

        if (!$request->user()->isActive()) {
            return redirect(route('auth.inactive'));
        }

        return redirect(route('void'));
    }
}
