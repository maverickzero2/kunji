<?php

namespace App\Http\Controllers\Void;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /*
     |--------------------------------------------------------------------------
     | The Void
     |--------------------------------------------------------------------------
     |
     | Something is not right. Perhaps something is not set up properly.
     | It could  be the User or Role,  or perhaps it is the object the
     | User  was  trying to  access.  Anyway, The system do not know
     | where to redirect the User and that's how the User ended up
     | here in the Void :( .....................................
     |
     */

    public function index()
    {
        return "You ended up here because something is not right. Contact the admin.";
    }
}
