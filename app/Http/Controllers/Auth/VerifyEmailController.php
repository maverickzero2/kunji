<?php

namespace App\Http\Controllers\Auth;

use App\Facades\Token;
use App\Http\Requests\Auth\ResendVerificationRequest;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Notification;
use App\Notifications\EmailVerification;
use Softon\SweetAlert\Facades\SWAL;

class VerifyEmailController extends Controller
{
    public function form(string $sent = null)
    {
        return view('auth.resend_verification', [
            'sent' => $sent,
        ]);
    }


    /**
     * Email will be resent if :
     * 1. Email found
     * 2. User's active
     * 3. User's yet verified
     * 4. Token not found & Token is expired.
     *
     * @param ResendVerificationRequest $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function resend(ResendVerificationRequest $request, User $user)
    {
        if (!$found = $user->where('email', $request->email)->first()) {
            return $this->sent();
        }
        if (!$found->isActive()) {
            return $this->sent();
        }

        if ($found->isVerified()) {
            return $this->sent();
        }

        if ($found->token && !$found->token->isExpired()) {
            return $this->sent();
        }

        Token::regenerate($found);

        Notification::send($found, new EmailVerification());
        return $this->sent();

    }

    public function verify(string $token)
    {

        if (!$user = Token::verify($token)) {

            SWAL::error('Invalid verification link', 'Verification link is expired not found. Please request for a new one');
            return redirect(route('verify.form'));

        }

        SWAL::success('Your account is verified', 'You can login with your email and password. :)');
        return redirect(route('auth.login'));

    }

    /**
     * Tell the user that the request will be sent IF the email is valid, without
     * revealing the actual information.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    private function sent()
    {
        return redirect(route('verify.form', ['sent' => '1']));
    }

}
