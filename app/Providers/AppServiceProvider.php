<?php

namespace App\Providers;

use App\Models\Option;
use App\models\Token;
use Illuminate\Support\ServiceProvider;
use function Psy\bin;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //

        $this->app->bind('Auth\Token', Token::class);

        $this->app->bind('Admin\Option', Option::class);

    }
}
