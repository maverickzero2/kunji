<?php

namespace App\Listeners;

use App\Notifications\EmailVerification;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Notification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserRegistration
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Object $registered
     * @return void
     */
    public function handle($registered)
    {
        Notification::send($registered->user, new EmailVerification());
    }
}
