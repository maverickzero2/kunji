<?php
/**
 * Created by PhpStorm.
 * User: cheow
 * Date: 30/12/2017
 * Time: 3:47 PM
 */

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Token extends Facade
{
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Auth\Token';
    }

}