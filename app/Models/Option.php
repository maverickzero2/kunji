<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $fillable = [
        'option_name',
        'value',
    ];

    public function getValue($option_name)
    {
        return $this->where('option_name', $option_name)->first()->value ?: null;
    }

}
