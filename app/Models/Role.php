<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class Role extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'flags', 'description'
    ];


    protected $casts = [
        'flags' => 'array',];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'name';
    }


    /*
     * Accessors
     */

    /**
     * @return mixed
     */
    public function getAdminAttribute()
    {
        return $this->where('name', config('constants.roles.admin'))->first();
    }


    /*
     * Mutators
     */

    /**
     * @param $name
     */
    public function setNameAttribute($name)
    {
        $this->attributes['name'] = strtolower($name);
    }

    /*
     * Relations
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Users()
    {
        return $this->hasMany(User::class);
    }


    /*
     * Determinators :)
     */

    /**
     * @return bool
     */
    public function hasRelation(): bool
    {
        if ($this->users()->exists()) {
            return true;
        }

        return false;

    }

    /**
     * @param array $flags
     * @return bool
     */
    public function hasFlags(array $flags): bool
    {
        if (empty($this->flags)) {
            return false;
        }

        return in_array($flags, $this->flags);
    }

    /**
     * @param string $column
     * @param array $values
     * @return Builder
     */
    public function whereArrayHas(string $column, array $values) : Builder
    {
        $query = $this;
        foreach ($values as $value) {
            $query = $query->where($column, 'LIKE', '%\"'.$value.'\"%');
        }

        return $query;
    }


    /**
     * @param string $column
     * @param array $values
     * @return Builder
     */
    public function whereNotArrayHas(string $column, array $values) : Builder
    {
        $query = $this;
        foreach ($values as $value) {
            $query = $query->where($column, 'NOT LIKE', '%\"'.$value.'\"%');
        }

        return $query;
    }

}
