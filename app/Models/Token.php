<?php

namespace App\models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Facades\Option;

class Token extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'token', 'user_id',
    ];


    /*
     * Relations
    */

    /**
     * user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /*
     * Determinators
     */

    public function isExpired()
    {
        $interval = Option::getValue('verification_sending_interval') ?: 0;
        return $this->created_at->addDay($interval) < (new Carbon());

    }

    /*
     * Methods
     */

    /**
     * Generate or regenerate token for user.
     *
     * @param User $user
     * @return mixed
     */
    public function regenerate(User $user)
    {

        if ($found = $this->where('user_id', $user->id)->first()) {
            $found->delete();
        }

        return $this->create([
            'user_id' => $user->id,
            'token' => str_random(25),
        ]);
    }

    public function verify(string $token)
    {
        if (!$found = $this->where('token', $token)->first()) {
            return false;
        }

        $user = $found->user;

        $user->flags = array_merge($user->flags, config('user.verified'));

        $user->save();

        $found->delete();

        return $user;
    }

}
