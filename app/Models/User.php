<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use
        Notifiable,
        SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'role_id', 'flags'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'flags' => 'array',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /*
     |--------------------------------------------------------------------------
     | Accessors
     |--------------------------------------------------------------------------
     |
     |
     |
     |
     |
     */

    public function isAdmin(): bool
    {
        return $this->role()->where('name', config('constants.roles.admin'))->exists();
    }

    public function isVerified()
    {
        return !(bool)array_diff(config('user.verified'), $this->flags);
    }

    public function isActive()
    {
        return !(bool)array_diff(config('user.active'), $this->flags);
    }

    /*
     |--------------------------------------------------------------------------
     | Finders
     |--------------------------------------------------------------------------
     |
     |
     |
     |
     |
     */

    public function findByEmail(string $email)
    {
        return $this->where('email', $email)->first();
    }

    /*
     |--------------------------------------------------------------------------
     | Relations
     |--------------------------------------------------------------------------
     |
     |
     |
     |
     |
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function token()
    {
        return $this->hasOne(Token::class);
    }

}
