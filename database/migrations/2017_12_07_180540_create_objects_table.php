<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('roles_ids')->default('[]');
            $table->string('attribute_group_ids')->default('[]');
            $table->string('interface')->default('[]');
            $table->string('flags')->default('[]');
            $table->string('type');
            $table->integer('parent')->default(0);
            $table->integer('level')->default(0);
            $table->integer('priority')->default(10);
            $table->softDeletes();
            $table->timestamps();

            $table->index(['parent']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objects');
    }
}
