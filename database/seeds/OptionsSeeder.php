<?php

use Illuminate\Database\Seeder;
use App\Models\Option;

class OptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Option $option)
    {
        $option->create([
            'option_name' => 'email_unverified',
            'value' => 'Please verify your email to activate your account. Click on the link in the verification email we sent to you. Unverified accounts will be removed from time to time.',
        ]);

        $option->create([
            'option_name' => 'user_inactive',
            'value' => 'You account is disabled.',
        ]);

        $option->create([
            'option_name' => 'verification_sending_interval',
            'value' => '1',
        ]);

        $option->create([
            'option_name' => 'user_default_flags',
            'value' => '["active"]',
        ]);
    }
}
