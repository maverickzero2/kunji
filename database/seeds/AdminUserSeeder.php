<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;

class AdminUserSeeder extends Seeder
{
    private
        $email = 'cheow@rerama.com',
        $password = 'venus';

    /**
     * Run the database seeds.
     * @param User $user
     * @param  Role $role
     *
     * @return void
     */
    public function run(User $user, Role $role)
    {

        if (!$role->admin) {
            $role->create([
                'name' => config('constants.roles.admin'),
                'flags' => [config('constants.roles.admin')],
            ]);
        }

        if (!$user->where('email', $this->email)->first()) {
            $user->create([
                'email' => $this->email,
                'password' => bcrypt($this->password),
                'flags' => ['I\'m the admin!'],
                'role_id' => $role->admin->id,
            ]);
        }
    }
}
