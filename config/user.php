<?php
/**
 * Created by PhpStorm.
 * User: cheow
 * Date: 31/12/2017
 * Time: 12:40 PM
 */

return [
    'valid' => [
        config('constants.flags.user.active'),
        config('constants.flags.user.verified'),
    ],
    'active' => [
        config('constants.flags.user.active'),
    ],
    'verified' => [
        config('constants.flags.user.verified'),
    ],
    'default' => [
        config('constants.flags.user.active'),
    ],
];