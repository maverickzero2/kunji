<?php
/**
 * Created by PhpStorm.
 * User: cheow
 * Date: 21/12/2017
 * Time: 10:04 PM
 */

return [

    'roles' => [
        'admin' => 'admin',
    ],

    'flags' => [
        'user' => [
            'verified' => 'verified',
            'active' => 'active',
        ],

        'role' => [
            'active' => 'active',
        ],

    ],

    'reserved' => [

        'roles' => [
            'admin',
        ],

        'object' => [
            'names' => [
                'user',
                'object',
                'admin',
                'role',
                'log',
                'password',
                'password_resets',
                'option',
                'session',
            ],

            'attributes' => [
                'id',
                'name',
                'roles_ids',
                'attribute_group_ids',
                'interface',
                'flags',
                'type',
                'parent',
                'level',
                'priority',
                'created_at',
                'updated_at',
                'delete_at',
            ],
        ],

    ],
    'options' => [
        'email_unverified',
        'user_inactive',
    ],
];