# Kunji

A web application management system. It is meant for crafting and managing non-transactional web applications such as job portal, listing, social media, blogs) This is where you craft and manage your web application. You can easily set up non-transactional objects like roles, post types, and their ownerships.

## Requirements

- PHP 7.1 and above
- MySQL 5.7 and above

### Dependencies

TBA

```
TBA
```

### Installing

TBA

```
TBA
```
TBA

```
TBA
```


## Contributing

TBA 

## Authors

Me. TBA

## License

TBA

## Acknowledgments

TBA
