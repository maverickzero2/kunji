<nav>
    <a class="list-group-item list-group-item-action" href="{{ route('admin.dashboard')  }}">
        <i class="fa fa-tachometer mr-2" fa-hidden="true"></i>
        <span class="d-none d-md-inline">Dashboard
        </span>
    </a>

    <a class="list-group-item list-group-item-action" href="{{ route('admin.objects.index') }}">
        <i class="fa fa-object-group mr-2" fa-hidden="true"></i>
        <span class="d-none d-md-inline">
            Objects
        </span>
    </a>

    <a class="list-group-item list-group-item-action" href="{{ route('admin.roles.index') }}">
        <i class="fa fa-users mr-2" fa-hidden="true"></i>
        <span class="d-none d-md-inline">
            Roles
        </span>
    </a>
</nav>
