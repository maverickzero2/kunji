<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') {{ config('app.name', 'Kunji') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<header>
    @include('components.nav')
</header>
<div class="container-fluid body">
    <div class="row">
        <div class="col-md-3 col-lg-2 sidebar mt-5 d-none d-md-block">
            <aside>
                <div id="member-sidebar" class="list-group">
                    @yield('sidebar')
                </div>
            </aside>
        </div>
        <div id="app" class="body col-md-9 col-lg-10 mt-5" role="main">
            <div class="container">
                @yield('content')
            </div>
        </div>
    </div>
</div>

<div class="footer fixed-bottom-md-down bg-secondary text-light">
    <footer>
        <div class="container p-3 text-center">
            @yield('footer')
        </div>
    </footer>
</div>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
@include('sweetalert::cdn')
@include('sweetalert::view')
</body>
</html>
