@extends('layouts.public')

@section('title', 'Email address not verified')

@section('content')
    <div class="alert alert-warning">
        <h2><i class="fa fa-envelope"></i> Your email address is not verified</h2>
        <p>
            Please verify your email address to activate you account.
        </p>
        <p>
            Click on the link in the verification email we sent to you.
        </p>
    </div>
    <div class="card">
        <div class="card-body">
            Did get the email? <a href="{{ route('verify.form') }}">Request new verification email</a>.
        </div>
    </div>
@endsection

@section('footer')
    @include('components.footer')
@endsection