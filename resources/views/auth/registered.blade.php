@extends('layouts.public')

@section('title', 'Registration Complete - ')

@section('content')
    <h1>Registration Successful</h1>
    <p>
        Please check your email {{ $user->email }} for further instruction.
    </p>
    <p>
        Thank you. :)
    </p>
@endsection

@section('footer')
    @include('components.footer')
@endsection