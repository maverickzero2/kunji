@extends('layouts.public')

@section('title', 'Request new verification email - ')

@section('content')

    <div class="card">
        <div class="card-body">
            <h1 class="card-title">
                Request new verification email
            </h1>
            <p class="card-text">
                You can request for new verification email here. Make sure you check your spam mailbox just in case our
                mail went there. :)
            </p>
            @if($errors->any())
                <div class="alert alert-danger small">
                    <h5>For your information:</h5>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="mt-1">

                @if(!empty($sent))

                    <div class="alert alert-info">
                        Verification email should be sent if the account exists. Please check your mail. Thank you. :)
                    </div>

                @else

                    <form class="form-horizontal {{ $errors->any() ? 'was-validated' : ''  }}" method="POST"
                          action="{{ route('verify.resend') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="email">
                                What's your E-Mail Address?
                            </label>

                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                                   required/>
                            <small>
                                The email you used for registering the account.
                            </small>

                            @if ($errors->has('email'))
                                <span class="help-block">
                        <strong>
                            {{ $errors->first('email') }}
                        </strong>
                    </span>
                            @endif
                        </div>


                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                Register
                            </button>
                        </div>
                    </form>

                @endif
            </div>
        </div>
    </div>
@endsection

@section('footer')
    @include('components.footer')
@endsection