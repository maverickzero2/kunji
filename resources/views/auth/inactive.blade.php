@extends('layouts.public')

@section('title', 'Account disabled')

@section('content')
    <div class="alert alert-danger">
        <h2><i class="fa fa-user-times"></i> Your account is disabled.</h2>
        <p>
            You are not allowed to login.
        </p>

    </div>
@endsection

@section('footer')
    @include('components.footer')
@endsection