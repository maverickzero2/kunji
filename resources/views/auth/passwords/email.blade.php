@extends('layouts.public')

@section('title', 'Reset Password - ')

@section('content')
    <div class="card">
        <div class="card-body">
            <h1 class="card-title">
                Reset Password
            </h1>

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <form class="form-horizontal {{ $errors->any() ? 'was-validated' : ''  }}" method="POST"
                  action="{{ route('password.email') }}">

                {{ csrf_field() }}
                <div class="form-group">
                    <label for="email" class="control-label">
                        E-Mail Address
                    </label>

                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                           required/>
                </div>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                        Send Password Reset Link
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
