<nav class="navbar navbar-dark bg-primary navbar-left navbar-expand-md" role="navigation">

    <a class="navbar-brand" href="{{ url('/') }}">
        {{ config('app.name', 'Laravel') }}
    </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse"
            data-target="#navbar-toggle-menu" aria-controls="navbar-toggle-menu" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>


    @guest
        <div class="collapse navbar-collapse" id="navbar-toggle-menu">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">
                        Login
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">
                        Register
                    </a>
                </li>
            </ul>

        </div>
    @else



        <div class="collapse navbar-collapse" id="navbar-toggle-menu">
            @if(!Auth::user()->isAdmin())
            <form class="form-inline navbar-form d-md-block">
                <div class="input-group input-group-sm">
                    <input name="search" type="text" class="form-control" placeholder="Search"
                           aria-label="Search" aria-describedby="search-addon"/>
                    <span class="input-group-addon" id="search-addon"><i class="fa fa-search"></i></span>
                </div>
            </form>

            @endif
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('logout') }}"
                       onclick="event.preventDefault();  document.getElementById('logout-form').submit();">
                        Logout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                          style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </div>

    @endguest

</nav>