<div class="d-none d-md-block">
    Kunji @ 2017
</div>
<div class="d-md-none">
    @guest
        {{ config('app.name', 'Kunji') }} @ {{ date('Y') }}
    @else
        @if(Auth::user()->isAdmin())
            @include('sidebars.admin')
        @else
            @include('sidebars.member')
        @endif
    @endguest
</div>