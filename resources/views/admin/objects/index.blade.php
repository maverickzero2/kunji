@extends('layouts.admin')

@section('content')
    <h4>
        <i class="fa fa-object-group"></i> Manage Objects
    </h4>
    <p class="text-justified text-secondary">
        Objects are things that can be posted by the users. Example: posts, photos, advertisements, listing, etc.
    </p>
    <p>
        <a class="btn btn-primary" href="{{ route('admin.objects.create')  }}">New Object</a>
    </p>
@endsection

@section('sidebar')
    @include('sidebars.admin')
@endsection

@section('footer')
    @include('components.footer')
@endsection