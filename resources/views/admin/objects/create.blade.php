@extends('layouts.admin')

@section('content')
    <h4>
        Create New Object
    </h4>
    <p class="text-justified text-secondary">
        Set up your object.
    </p>
    <p>
        <a class="btn btn-primary" href="#submit">Create</a>
    </p>
@endsection

@section('sidebar')
    @include('sidebars.admin')
@endsection

@section('footer')
    @include('components.footer')
@endsection