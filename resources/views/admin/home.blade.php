@extends('layouts.admin')

@section('title', 'Admin - ')

@section('content')
    <h4>
        Behold. The Administrator Dashboard!
    </h4>
    <p class="text-justified text-secondary">
        This is where you craft the system.
    </p>
@endsection

@section('sidebar')
    @include('sidebars.admin')
@endsection

@section('footer')
    @include('components.footer')
@endsection