@extends('layouts.admin')
@section('title', 'Roles - ')
@section('content')
    <h4>
        <i class="fa fa-users"></i> Manage Roles
    </h4>
    <p class="text-justified text-secondary">
        Role used for categorising registered users, and for managing their object ownerships and authorisation.
    </p>
    <p>
        <a class="btn btn-sm btn-primary" href="{{ route('admin.roles.create') }}">
            Add New Role
        </a>
    </p>

    <table class="table table-info table-light table-striped">
        <thead>
        <tr>
            <th>
                #
            </th>
            <th>
                Roles
            </th>
            <th>
                Flags
            </th>
            <th>
                Actions
            </th>
        </tr>
        </thead>
        <tbody>
        @forelse($roles as $role)
            <tr>
                <td>
                    {{ $loop->iteration }}
                </td>
                <td>
                    {{ $role->name }}
                </td>
                <td>
                    @each('components.badge', $role->flags, 'badge')
                </td>
                <td>
                    @unless($role->name === 'admin')

                        <a href="{{ route('admin.roles.edit', ['role' => $role]) }}" class="badge">
                            <i class="fa fa-pencil"></i>
                            <span class="d-none d-md-inline">Edit</span>
                        </a>
                        <form class="inline" action="{{ route('admin.roles.destroy', [ 'role' => $role ]) }}"
                              method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button class="btn btn-link badge text-danger">
                                <i class="fa fa-times"></i>
                                <span class="d-none d-md-inline">Delete</span>
                            </button>
                        </form>
                    @endunless
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="4" class="text-center text-muted">
                    There is no Role at the moment. Let's <a href="{{ route('admin.roles.create') }}">create a new
                        role</a> now! <i class="fa fa-smile-o"></i>
                </td>
            </tr>
        @endforelse
        </tbody>
    </table>
    <a class="text-danger" href="{{ route('admin.roles.trash') }}"><i class="fa fa-trash"></i> View Trash (Roles)</a>
@endsection

@section('sidebar')
    @include('sidebars.admin')
@endsection

@section('footer')
    @include('components.footer')
@endsection