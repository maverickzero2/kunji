@extends('layouts.admin')
@section('title', 'Trash (Roles) - ')
@section('content')
    <h4 class="text-danger">
        <i class="fa fa-trash"></i> Trash (Roles)
    </h4>
    <p class="text-justified text-secondary">
        Deleted Roles.
    </p>
    <p class="text-right">
        <a class="btn btn-sm btn-danger" href="{{ route('admin.roles.create') }}">
            Empty Trash
        </a>
    </p>

    <table class="table table-info table-light table-striped">
        <thead class="bg-warning">
        <tr>
            <th>
                #
            </th>
            <th>
                Roles
            </th>
            <th>
                Flags
            </th>
            <th>
                Actions
            </th>
        </tr>
        </thead>
        <tbody>
        @forelse($roles as $role)
            <tr>
                <td>
                    {{ $loop->iteration }}
                </td>
                <td>
                    {{ $role->name }}
                </td>
                <td>
                    @each('components.badge', $role->flags, 'badge')
                </td>
                <td>

                    <a href="{{ route('admin.roles.restore', ['role' => $role->name]) }}" class="badge">
                        <i class="fa
                            fa-mail-reply"></i>
                        <span class="d-none d-md-inline">Restore</span>
                    </a>
                    <a href="{{ route('admin.roles.erase', ['role' => $role->name]) }}" class="badge text-danger">
                        <i class="fa
                            fa-bomb"></i>
                        <span class="d-none d-md-inline">Erase</span>
                    </a>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="4" class="text-center text-muted">
                    There is no Role in the trash at the moment. <i class="fa fa-smile-o"></i>
                </td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection

@section('sidebar')
    @include('sidebars.admin')
@endsection

@section('footer')
    @include('components.footer')
@endsection