@extends('layouts.admin')
@section('title', 'Create Role - ')
@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <i class="fa fa-user-plus"></i> Create New Role
            </h4>

            @if($errors->any())
                <div class="alert alert-danger small">
                    <h5>For your information:</h5>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="POST" action="{{ route('admin.roles.store') }}"
                  class="form-horizontal {{ $errors->any() ? 'was-validated' : ''  }}">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="name">
                        Name
                    </label>

                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"
                           required/>
                    <small>
                        Alphabets only. This will be saved in lowercase.
                    </small>
                </div>

                <div class="form-group">
                    <label for="description">
                        Description
                    </label>

                    <textarea id="description" type="text" class="form-control" name="description" >{{ old('description') }}</textarea>
                    <small>
                        This will shown to the user. Please explain this role.
                    </small>
                </div>

                <div class="form-group">
                    <label for="flags">
                        Flags
                    </label>
                    <div>
                        @foreach(config('constants.flags.role') as $flag)
                            <label>
                                <input name="flags[]" type="checkbox" value="{{ $flag }}"/> {{ $flag }}
                            </label>
                            <br />
                        @endforeach
                    </div>
                </div>

                <div class="form-group">
                    <button class="btn btn-primary">Create</button>
                </div>

            </form>
        </div>
    </div>
@endsection

@section('sidebar')
    @include('sidebars.admin')
@endsection

@section('footer')
    @include('components.footer')
@endsection