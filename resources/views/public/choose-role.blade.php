@extends('layouts.public')

@section('title', 'Register - ')

@section('content')

    <div class="card">
        <div class="card-body">
            <h1 class="card-title">
                Register
            </h1>
            <p class="card-text">
                The base user registration kept simple using email as username. Everything else considered as profile
                data, and can be added and customized according to user group.
            </p>
            @if($errors->any())
                <div class="alert alert-danger small">
                    <h5>For your information:</h5>
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
                </div>
            @endif
            <div class="mt-1">
                <form class="form-horizontal {{ $errors->any() ? 'was-validated' : ''  }}" method="POST"
                      action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="email">
                            E-Mail Address
                        </label>

                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                               required/>
                        <small>
                            This is the communication email as well as the unique login ID.
                        </small>

                        @if ($errors->has('email'))
                            <span class="help-block">
                        <strong>
                            {{ $errors->first('email') }}
                        </strong>
                    </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="password">
                            Password
                        </label>


                        <input id="password" type="password" class="form-control" name="password" required/>

                        @if ($errors->has('password'))
                            <span class="help-block">
                        <strong>
                            {{ $errors->first('password') }}
                        </strong>
                    </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="password-confirm">
                            Confirm Password
                        </label>

                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                               required/>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">
                            Register
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    @include('components.footer')
@endsection