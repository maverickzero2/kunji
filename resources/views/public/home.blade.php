@extends('layouts.public')

@section('content')
    <h4>
        Basic setup for web applications.
    </h4>
    <p class="text-justified text-secondary">
        Kunji comes with an users authentication and management system. Users can post information and messaging each
        other by default. Use Kunji to setup your web application. Categorise the user roles, create different post
        types, and customise the templates to suit your project.
    </p>
    <p>
        <a class="btn btn-primary" href="{{ route('login') }}">Login</a>
        <a class="btn btn-secondary" href="{{ route('register') }}">Register</a>
    </p>
@endsection

@section('footer')
    @include('components.footer')
@endsection