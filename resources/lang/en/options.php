<?php
/**
 * Created by PhpStorm.
 * User: cheow
 * Date: 30/12/2017
 * Time: 9:44 PM
 */

return [
    'options' => [
        'email_unverified_notice' => 'Email Unverified Notice',
        'user_inactive_notice' => 'Account Inactive Notice',
    ]
];