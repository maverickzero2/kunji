<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
 * Logout routes
 */
Route::get('/logout','Auth\LoginController@logout')->name('logout');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

/*
 * Logged in redirection
 */
Route::get('/home', 'HomeController@index')->name('home');

/*
 * Email verification routes
 */
Route::group([ 'prefix' => 'email', 'as' => 'verify.', 'middleware' => ['guest'] ], function () {
    Route::get('/verify/{sent?}', 'Auth\VerifyEmailController@form')->name('form');
    Route::post('/verify/', 'Auth\VerifyEmailController@resend')->name('resend');
    Route::get('/verify/token/{token}', 'Auth\VerifyEmailController@verify')->name('email');
});

/*
 * Guest ONLY Routes
 */
Route::group(['middleware' => ['guest'] ], function () {

    Route::get('/', 'FrontController@index')->name('front');

    Route::get('/unverified','Auth\LoginController@notVerified')->name('auth.unverified');
    Route::get('/inactive','Auth\LoginController@notActive')->name('auth.inactive');

    Route::get('login/{notice?}', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login')->name('auth.do-login');


// Registration Routes...
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register')->name('auth.do-register');

// Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
});




/*
 * The Admin's Routes
 */

Route::group([ 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['admin'] ], function () {

    Route::get('/', 'Admin\DashboardController@index')->name('dashboard');

    Route::resource('objects', 'Admin\ObjectController');

    route::get('/roles/trash', 'Admin\RoleController@trash')->name('roles.trash');
    route::get('/roles/{id}/restore', 'Admin\RoleController@restore')->name('roles.restore');
    route::get('/roles/{id}/erase', 'Admin\RoleController@erase')->name('roles.erase');
    Route::resource('roles', 'Admin\RoleController');

});

Route::get('/void', 'Void\HomeController@index')->name('void');